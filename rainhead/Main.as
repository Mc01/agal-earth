package rainhead
{
	import com.adobe.utils.AGALMiniAssembler;
	import com.adobe.utils.PerspectiveMatrix3D;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DTextureFormat;
	import flash.display3D.Context3DTriangleFace;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.textures.Texture;
	import flash.display3D.VertexBuffer3D;
	import flash.events.Event;
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	import flash.utils.ByteArray;
	import flash.utils.getTimer;
	import net.hires.debug.Stats;
	
	/**
	 * ...
	 * @author Mc
	 */
	public class Main extends Sprite 
	{
		//Screen
		private const initialWidth:int = 300;
		private const initialHeight:int = 300;
		//Vertices
		private const attributes:int = 5;
		private const verticesInQuad:int = 4;
		private const radius:Number = 0.5;
		private const leftFace:Vector.<Number> = Vector.<Number>([
			-radius, -radius, +radius, 0.00, 0.50,
			-radius, -radius, -radius, 0.25, 0.50,
			-radius, +radius, +radius, 0.00, 0.25,
			-radius, +radius, -radius, 0.25, 0.25
		]);
		private const frontFace:Vector.<Number> = Vector.<Number>([
			-radius, -radius, -radius, 0.25, 0.50,
			+radius, -radius, -radius, 0.50, 0.50,
			-radius, +radius, -radius, 0.25, 0.25,
			+radius, +radius, -radius, 0.50, 0.25
		]);
		private const rightFace:Vector.<Number> = Vector.<Number>([
			+radius, -radius, -radius, 0.50, 0.50,
			+radius, -radius, +radius, 0.75, 0.50,
			+radius, +radius, -radius, 0.50, 0.25,
			+radius, +radius, +radius, 0.75, 0.25
		]);
		private const backFace:Vector.<Number> = Vector.<Number>([
			+radius, -radius, +radius, 0.75, 0.50,
			-radius, -radius, +radius, 1.00, 0.50,
			+radius, +radius, +radius, 0.75, 0.25,
			-radius, +radius, +radius, 1.00, 0.25
		]);
		private const bottomFace:Vector.<Number> = Vector.<Number>([
			-radius, -radius, +radius, 0.25, 0.75,
			+radius, -radius, +radius, 0.50, 0.75,
			-radius, -radius, -radius, 0.25, 0.50,
			+radius, -radius, -radius, 0.50, 0.50
		]);
		private const topFace:Vector.<Number> = Vector.<Number>([
			-radius, +radius, -radius, 0.25, 0.25,
			+radius, +radius, -radius, 0.50, 0.25,
			-radius, +radius, +radius, 0.25, 0.00,
			+radius, +radius, +radius, 0.50, 0.00
		]);
		//Shaders
		private const vertexCode:String = 
			"m44 op, va0, vc0\n" + //Output position = VertexBuffer ** Matrix of transforms
			"mov v0, va1\n" //Pass UVs to FragmentShader
		;
		private const fragmentCode:String = 
			"text oc v0, fs0 <2d, linear, nomip, repeat>\n" //Output color = Texture from UVs and FragmentSampler
		;
		//Texture
		[Embed(source = "../../bin/earth.png")]
		private const textureClass:Class;
		private const textureBitmap:Bitmap = new textureClass();
		private var texture:Texture;
		//Context3D
		private var context:Context3D;
		private var vertexData:Vector.<Number>;
		private var indexData:Vector.<uint>;
		private var vertices:int;
		private var indicies:int;
		private var vertexBuffer:VertexBuffer3D;
		private var indexBuffer:IndexBuffer3D;
		private var program:Program3D;
		private var vertexShader:ByteArray;
		private var fragmentShader:ByteArray;
		private var perspectiveMatrix:PerspectiveMatrix3D;
		private var transformMatrix:Matrix3D;
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			//Request the context
			stage.stage3Ds[0].addEventListener(Event.CONTEXT3D_CREATE, onContext3D);
			stage.stage3Ds[0].requestContext3D();
		}
		
		private function onContext3D(e:Event):void 
		{
			removeEventListener(Event.CONTEXT3D_CREATE, onContext3D);
			
			//Init the context
			context = stage.stage3Ds[0].context3D;
			context.configureBackBuffer(initialWidth, initialHeight, 4);
			context.setCulling(Context3DTriangleFace.BACK);
			
			//Init data
			initTexture();
			initData();
			
			//Assemble shaders with buffers
			createBuffers();
			createShaders();
			
			//Upload data to GPU
			uploadBuffers();
			uploadShaders();
			
			//Begin rendering
			startProgram();
			addChild(new Stats());
			this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function initTexture():void 
		{
			texture = context.createTexture(512, 512, Context3DTextureFormat.BGRA, false);
			var textureData:BitmapData = textureBitmap.bitmapData;
			texture.uploadFromBitmapData(textureData);
			context.setTextureAt(0, texture);
		}
		
		private function initData():void 
		{
			perspectiveMatrix = new PerspectiveMatrix3D();
			transformMatrix = new Matrix3D();
			//Concat faces
			vertexData = leftFace.concat(frontFace, rightFace, backFace, bottomFace, topFace);
			//Rise subdivided faces into sphere
			vertexData = riseVertices(fractureRecursively(vertexData, 2));
			vertices = vertexData.length / attributes;
			indicies = 3 * vertices / 2;
			indexData = createIndices(indicies);
		}
		
		private function fractureRecursively(inputFace:Vector.<Number>, number:int = 0):Vector.<Number> {
			if (number > 0) return fractureRecursively(fractureVertices(inputFace), number - 1);
			else return fractureVertices(inputFace);
		}
		
		private function fractureVertices(inputFace:Vector.<Number>):Vector.<Number>
		{
			var outputFace:Vector.<Number> = new Vector.<Number>();
			var limit:int = inputFace.length;
			var attributesInQuad:int = attributes * verticesInQuad;
			//Quad in vector
			for (var i:int = attributesInQuad - 1; i < limit; i = i + attributesInQuad) 
			{
				var currentQuad:int = i - (attributesInQuad - 1);
				//Vertices in quad
				for (var j:int = 0; j < verticesInQuad; j++) {
					//Subdivide vertex into four
					for (var k:int = 0; k < verticesInQuad; k++) {
						//Iterate through attibutes
						for (var l:int = 0; l < attributes; l++) {
							//Average
							var value:Number = inputFace[currentQuad + j * attributes + l];
							value += inputFace[currentQuad + k * attributes + l];
							outputFace.push(value / 2)
						}
					}
				}
			}
			return outputFace;
		}
		
		private function riseVertices(inputFace:Vector.<Number>):Vector.<Number> 
		{
			var outputFace:Vector.<Number> = new Vector.<Number>();
			var limit:int = inputFace.length;
			for (var i:int = attributes - 1; i < limit; i = i + attributes) 
			{
				var x:Number = inputFace[i - 4];
				var y:Number = inputFace[i - 3];
				var z:Number = inputFace[i - 2];
				var distance:Number = Math.sqrt(x * x + y * y + z * z);
				
				//Push XYZ
				x = x * radius / distance;
				y = y * radius / distance;
				z = z * radius / distance;
				
				//Pass XYZ and UVs
				outputFace.push(x, y, z);
				outputFace.push(inputFace[i - 1], inputFace[i]);
			}
			return outputFace;
		}
		
		private function createIndices(limit:int):Vector.<uint> 
		{
			var indexData:Vector.<uint> = new Vector.<uint>();
			for (var i:int = verticesInQuad - 1; i < limit; i = i + verticesInQuad) {
				//Creation of quad
				indexData.push(i - 1, i - 2, i - 3);
				indexData.push(i - 2, i - 1, i);
			}
			return indexData;
		}
		
		private function createBuffers():void 
		{
			vertexBuffer = context.createVertexBuffer(vertices, attributes);
			indexBuffer = context.createIndexBuffer(indicies);
		}
		
		private function createShaders():void 
		{
			//Program
			program = context.createProgram();
			var assembler:AGALMiniAssembler = new AGALMiniAssembler();
			//Assemble
			vertexShader = assembler.assemble(Context3DProgramType.VERTEX, vertexCode);
			fragmentShader = assembler.assemble(Context3DProgramType.FRAGMENT, fragmentCode);
		}
		
		private function uploadBuffers():void 
		{
			//Upload to Buffers
			vertexBuffer.uploadFromVector(vertexData, 0, vertices);
			indexBuffer.uploadFromVector(indexData, 0, indicies);
		}
		
		private function uploadShaders():void 
		{
			//Upload to Program
			program.upload(vertexShader, fragmentShader);
		}
		
		private function startProgram():void 
		{
			//XYZ to VA0 and UVs to VA1
			context.setVertexBufferAt(0, vertexBuffer, 0, Context3DVertexBufferFormat.FLOAT_3);
			context.setVertexBufferAt(1, vertexBuffer, 3, Context3DVertexBufferFormat.FLOAT_2);
			context.setProgram(program);
		}
		
		private function onEnterFrame(e:Event):void 
		{
			context.clear(1, 1, 1);
			updateContext();
			context.drawTriangles(indexBuffer);
			context.present();
		}
		
		private function updateContext():void 
		{
			//FoV
			perspectiveMatrix.identity();
			perspectiveMatrix.perspectiveFieldOfViewLH(45 * Math.PI / 180, initialWidth / initialHeight, 0.1, 10);
			//Transforms
			transformMatrix.identity();
			//transformMatrix.appendRotation(getTimer() / 15, Vector3D.X_AXIS);
			transformMatrix.appendRotation(getTimer() / 15, Vector3D.Y_AXIS);
			//transformMatrix.appendRotation(getTimer() / 15, Vector3D.Z_AXIS);
			transformMatrix.appendTranslation(0, 0, 3);
			transformMatrix.append(perspectiveMatrix);
			context.setProgramConstantsFromMatrix(Context3DProgramType.VERTEX, 0, transformMatrix, true);
		}
	}	
}